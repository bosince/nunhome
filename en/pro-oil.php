<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>XTIUSD - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/oil01.jpg) no-repeat right 100px #fbfbfb ;background-size: contain; height: 560px;padding-top: 110px;filter:alpha(opacity=90); -moz-opacity:0.9; -khtml-opacity: 0.9;  opacity: 0.9;  }
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 42px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/oil03.png) no-repeat;background-size: cover;height: 500px;filter:alpha(opacity=90); -moz-opacity:0.9; -khtml-opacity: 0.9;  opacity: 0.9; }

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}

            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 500px;
                padding-top: 60px;
                background: none;
                background-color: #f4f4f4;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">XTIUSD</li>
            </ol>
        </div>
        <div class="bc333">
            <div class="introduce  ">
                <div class="container"> 
                    <div class="row">   
                        <div class="col-xs-12 col-md-6">    
                            <h2 class="c333 tc">XTIUSD</h2>
                            <p class="c333 mt50 max768-lh23 max768-mt15">XTIUSD is one of the world's most important natural resources, but also one of the most frequently traded commodities. XTIUSD can be traded on the NUN, in particular the quality of crude oil as the transaction object spot contract, is a CFD transaction, not only relates to the product itself, to bond as risk protection, is the implementation of T 0 trading system, can buy or buy, no change in the degree of risk, small, flexible delivery, suitable for cash. For investors, the investment of more than other types of transactions, earnings are high. At present, NUN offers customers the market wide trading in US crude oil (West Texas oil) and Brent crude oil, investors only need one can easily account for the transaction.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/oil02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why choose trading XTIUSD?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20 max768-fs16 c666">
                        <li>Transaction cost is low</li>
                        <li>Two way trading has hedging function</li>
                        <li>No board limit, small risk</li>
                        <li>Price fluctuations are affected by supply and demand, the market is easy to grasp</li>
                        <li>Quick execution, no repeat offer</li>
                        <li>A full stop of orders and instructions to control the position</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data </h2>
                <p class="tc plr15 mt30">With NUN trading in XTIUSD, customers can enjoy advanced trading tools, highly competitive spreads, and efficient execution speed, both bull and bear markets are profitable opportunities</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tbody>
                            <tr>
                                <td>Transaction currency </td>
                                <td>Minimum number of<br> transactions</td>
                                <td>Maximum number of<br> transactions</td>
                                <td>Contract unit</td>
                                <td>Order distance</td>
                                <td>Bail scale </td>
                                <td>Business time<br>（Beijing time）</td>
                                <td>Business time<br>（MT4 time）</td>
                            </tr>
                            <tr>
                                <td>USO/USD</td>
                                <td>0.1</td>
                                <td>20</td>
                                <td>1,000</td>
                                <td>2.4</td>
                                <td>2%</td>
                                <td>Weekdays：07:00-05:59</td>
                                <td>Weekdays：01:00-24:00</td>
                            </tr>
                            <tr>
                                <td>UKO/USD</td>
                                <td>0.1</td>
                                <td>20</td>
                                <td>1,000</td>
                                <td>2.4</td>
                                <td>2%</td>
                                <td>Weekdays：09:00-05:59</td>
                                <td>Weekdays：03:00-24:00</td>
                            </tr>           
                        </tbody>
                    </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>NUN remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>
        <div class="bc000 hidden-xs">
            <div class="account-intro c333 tc">
                <div class="container"> 
                    <div class="row">   
                        <div class="col-xs-12 col-md-6 col-md-offset-6">    
                            <h2 style="margin-top: 150px;">Account</h2>
                            <p class="c333 mt50 fs20">Now, Easy get your account type</p>
                            <a href="#" class="dib mt50 c333">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>