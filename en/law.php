<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Terms and Conditions - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">


    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    
   <style>
        .art_style h3{line-height: 50px;}
        .art_style p{line-height: 30px;}
        .art_style a{line-height: 30px; font-size: 16px;}

    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  tc pt120">
            <h2 class="cfff  tc fs40  ffwryh">Terms and Conditions</h2>
        </div>


        
        <div class="container">
            <div class="ffwryh c666 art_style mt50 mb50">

                <p>NUN is a regulated forex and CFD broker. We provide an all-around professional online trading platform for all traders. We offer world class financial derivatives trading through our top tier technologies and services.</p>
                
                <p>When you trade Forex and CFDs, you need to understand their complexity and high risk. It does not suitable for all investors. It depends on whether you are suitable for trading foreign exchange on margin and CFDs, and you should not invest without knowing the risks of these transactions. If you have any questions, it is important for you to look for an objective and professional advice.The followings are some of risks of trading CFDs. The product and services guide contains more information about risks, so make sure you read it before you open an account.</p>

                <div class="panel-group mt15" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a class="dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          1.NUN does not provide any personal advice
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        We only provide general product information. Therefore, before you apply for an account, you must consider your investment objectives, financial situation, and needs,as well as trading margin foreign exchange and CFD contain high-risk. We recommend that you read our Product Disclosure Statement and Financial Services Guide carefully, and consult your independent financial adviser, tax adviser, and other professional advisers.We cannot guarantee the results of your trading.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          2.You are not trading the underlying asset
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        When you trade margin Forex and CFDs, your profits or losses depend on a rise or fall in the price of the underlying product. You need to know that you do not have any interest in the underlying forex, indices, and commodities. You are trading CFDs based on price fluctuation. There is no physical exchange or commodity trading. CFDs are financial derivative product.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          3.Over-the-counter（OTC）Financial Derivatives
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        When you open a trade on our platform, you are in the OTC derivative contract, and orders cannot be transferred. This means that you are dealing directly with us, and these trades (positions) can only be closed with us. In other words, foreign exchange and CFD contracts are issued by NUN, not through any exchanges, such as the US Stock Exchange.NUN is your product issuer.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          4.Leverage
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        When trading FX, you only need a small margin to open a position. For example, if you trade AUD/USD with a value of $10,000 and a margin ratio of 0.5%, then you only need $50 to open a position. However, your risk in the market is $10,000. If your position gained 10%, you will earn $1,000. If you lose 10% of your position, you lose $1,000. The profit or loss of your trading depends on the size of the positions you open. NUN’s leverage is provided by the liquidity banks, so NUN reserves the right to adjust leverage at our own discretion.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          5.Market volatility
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                      <div class="panel-body">
                        Execution prices are based on the prices provided by our liquidity banks. The price of your trading products may fluctuate rapidly due to financial market news. Any changes in prices and spreads will have a direct impact on your account funds and positions. Price fluctuations can lead to a common situation called gapping, which occurred when opening price hugely differ from its closing price. Gapping caused by an unexpected economic event or a market announcement, especially when such information occurs outside the trading hours. Therefore, you may not have the opportunity to open or close positions between the two prices. The platform will execute your order at the next closest market price. You must bear the risk of a price gap, and your loss may exceed your account's net worth, which may cause your account go into negative balance. You have the responsibility to avoid negative account balance. NUN reserves the right to take further actions to recover the negative amount.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                          6.Slippage
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                      <div class="panel-body">
                        Slippage is also a risk that traders will face during trading. It is the result of rapid price fluctuations, the actual executed price of your trade differ from your pre-set execution price. NUN implements straight through processing(STP) model, and all the orders are executed at the price provided by our clearing banks, so traders must understand slippage can occur in foreign exchange trading. In addition, your pending orders (including pending order for new trades or pending order to close existing trades) price is for reference only, and the final traded price is the bank's actual execution price. You need to understand that in a fast-moving market, the actual result of a pending order may differ from your pre-set execution price.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                         7.Mandatory liquidation risk
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                      <div class="panel-body">
                        At any time, the existing funds in your account must remain above the mandatory liquidation level (the margin level must be above 50%), otherwise your open positions will be closed. However, please do not rely entirely on the system's liquidation order. It is your responsibility to manage your positions, account balances and account equity on your trading platform. To prevent a mandatory liquidation, you should have sufficient funds in your account. Note that even if the money you deposited earlier was sufficient at that time, it could quickly become insufficient due to the fast movement in the market. In addition, in the case of rapid market fluctuations, the hedging trades (locked positions) may also trigger mandatory liquidation. Traders should only open appropriate positions based on their available fund.
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                          8.Counterparty risk
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                      <div class="panel-body">
                        When you open an account with us and open a margin position, you enter into a CFD contract, we are your trading counterparty. That means we may not be able to fulfill our contractual obligations under unexpected circumstances, because of we or our own trading partners (such as our hedging providers) default. Under the impact of uncontrollable factors, we cannot guarantee the execution of your trading contract. In addition, if your trading methods and trading strategies that are against our risk control policies, we have the right to cancel your completed trades including your trading profits and agent commissions.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNine">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                          9.Client capital risk
                        </a>
                      </h4>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                      <div class="panel-body">
                        All customer funds are segregated. This means that all customers' deposits are kept in Client Trust Account, which separate from our own operating funds. However, in uncontrollable situations, your funds will still be at risk.
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTen">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                          10.Technical risk and other circumstances that affect your trades
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                      <div class="panel-body">
                        Some circumstances may prevent you from executing orders or from logging into our trading platform or instantly deposit funds into your account. This includes system errors or termination of supply, platform maintenance, network connectivity issues, or some third-party failures (such as network vendors,power companies and payment providers) that you or we cannot control. We have an emergency solution on these issues, but sometimes you may still not able to enter the trading platform or deposit funds electronically. These technical risks and contingencies pose a risk when you want to open or close positions.
                      </div>
                    </div>
                  </div>

                </div>


                <!-- <div class="ml15">
                    <p><a href="/assets/pdf/NUN金融关于订单执行政策.pdf" target="_bank">NUN order execution policy</a></p>
                    <p><a href="/assets/pdf/NUN金融关于反洗钱的规定.pdf" target="_bank">NUN anti-money laundering rules</a></p>
                    <p><a href="/assets/pdf/NUN金融关于风险揭示和警告通知.pdf" target="_bank">NUN notice of risk</a></p>
                    <p><a href="/assets/pdf/NUN金融关于客户投诉程序.pdf" target="_bank">NUN customer Complaint Handling</a></p>
                    <p><a href="/assets/pdf/NUN金融关于客户问卷调查.pdf" target="_bank">NUN customer questionnaire investigation</a></p>
                    <p><a href="/assets/pdf/NUN金融关于利益冲突政策.pdf" target="_bank">NUN conflict of Interest Policies</a></p>
                    <p><a href="/assets/pdf/NUN金融关于投资者赔偿基金.pdf" target="_bank">NUN investor compensation fund</a></p>
                    <p><a href="/assets/pdf/NUN金融关于网站使用条款.pdf" target="_bank">NUN terms of use website</a></p>
                    <p><a href="/assets/pdf/NUN金融关于隐私与安全政策.pdf" target="_bank">NUN safety and privacy</a></p>
                    <p><a href="/assets/pdf/NUN金融为客户提供服务的条款和条件.pdf" target="_bank">NUN terms and Conditions of Business</a></p>
                </div> -->
            </div> 
        </div>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <!-- <script type="text/javascript" src="assets/js/jquery/jquery.min.js"></script> -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>

</body>
</html>