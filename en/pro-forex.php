<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>G10 - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/introduce.jpg);background-size: cover; height: 560px;padding-top: 60px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 45px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/03.jpg) no-repeat 0 79%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 600px;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}

        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">G10</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">Mainstream plate</h2>
                        <p class="cfff mt30 max768-lh23 max768-mt15">The foreign exchange market is the world's largest financial market trading volume, according to the bank for International Settlements (BIS) statistics, in 2013 the global foreign exchange trading volume has reached 5 trillion and 300 billion, and continued to show growth. Compared with stocks, bonds and other financial market, foreign exchange market has 24 hours of continuous trading, fair and transparent, high liquidity the lever mechanism, low transaction cost advantages, and thus become the people competing sought after investment in the field of.NUN can provide 28 kinds of foreign exchange Angus international currency against the mainstream, for customers including the US dollar, euro, pound, yen, dollar, investors can choose one or more groups according to currency trading habits or experience against the dispersed investment.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why do you choose the foreign exchange mainstream?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20 max768-fs16 c666">
                        <li>T+0 straddle two-way choice</li>
                        <li>A small broad margin mechanism.</li>
                        <li>The low transaction cost, no fee</li>
                        <li>Big money is difficult to control the market</li>
                        <li>Lucrative</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data</h2>
                <p class="tc plr15 mt30">NUN with floating point, customers in the NUN platform to experience exciting to low poor, and not repeat the offer. In addition, NUN also provides a flexible lever up to 100:1, effectively help traders to achieve maximum benefits.</p>
                <div class="table-responsive">
                
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tr>
                            <th>Transaction currency </th>
                            <th>Minimum number <br>of transactions</th>
                            <th>Maximum number <br>of transactions</th>
                            <th>Contract unit</th>
                            <th>Order distance</th>
                            <th>Bail scale</th>
                            <th>Business time <br>(Beijing time)</th>
                            <th>Business time<br>（MT4 time）</th>
                        </tr>
                        <tr>
                            <td>EUR/USD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>2.4</td>
                            <td>0.5%~2%</td>
                            <td>Weekdays：06:05-05:59</td>
                            <td>Weekdays：00:05-24:00</td>
                        </tr>
                        <tr>
                            <td>USD/JPY</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>2.4</td>
                            <td>0.5%~2%</td>
                            <td>Weekdays：06:05-05:59</td>
                            <td>Weekdays：00:05-24:00</td>
                        </tr>
                        <tr>
                            <td>GBP/USD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>2.4</td>
                            <td>0.5%~2%</td>
                            <td>Weekdays：06:05-05:59</td>
                            <td>Weekdays：00:05-24:00</td>
                        </tr>
                        <tr>
                            <td>USD/CHF</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>2.4</td>
                            <td>0.5%~2%</td>
                            <td>Weekdays：06:05-05:59</td>
                            <td>Weekdays：00:05-24:00</td>
                        </tr>
                    </table>

                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>NUN remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
                <div class="tc mt30">
                    <a href="/en/pro-forex-more.php" class="dib btn btn-default w200 h50 lh35" >Show More</a>
                </div>
            </div>
        </div>

        <div class="account-intro cfff tc hidden-xs">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">Account</h2>
                        <p class="cfff mt50 fs20">Now, Easy get your account type</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">Read More</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>