<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>XAUUSD - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/gold01.jpg) no-repeat 0 50%;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/gold03.jpg) no-repeat 0 80%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}

            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 600px;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">XAUUSD</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">XAUUSD</h2>
                        <p class="cfff mt50">XAUUSD is one of the most important financial products is also a rare metal, has a very high economic value. In the period of financial turmoil, XAUUSD is a high coefficient of risk averse investors. The foreign exchange market in the spot gold may not need to physical delivery, using gold to ensure the dollar relative to the price of buying or form the relationship between gold and the dollar sold. In view of the shift, many investors will choose to buy gold as a way to circumvent the risk of US dollar.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/gold02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why choose trading XAUUSD?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20  max768-fs16 c666">
                        <li>With a reserve value</li>
                        <li>Stable and safe, low risk, high return</li>
                        <li>Can be used as a hedging strategy</li>
                        <li>Time delivery, easy to be realized</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data</h2>
                <p class="tc plr15 mt30">The NUN can provide up to 100:1 gold trading for customers to leverage, reasonable spreads, international bank optimal quotations, contracts and flexible size, and let the customer enjoy multiple instructions and place an order risk management.</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tbody>
                            <tr>
                                <td>Transaction currency </td>
                                <td>Minimum number <br>of transactions</td>
                                <td>Maximum number<br> of transactions</td>
                                <td>Contract unit</td>
                                <td>Order distance</td>
                                <td>Bail scale </td>
                                <td>Business time<br>（Beijing time）</td>
                                <td>Business time<br>（MT4 time）</td>
                            </tr>
                            <tr>
                                <td>XAU/USD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100</td>
                                <td>2.4</td>            
                                <td>0.5%~2%</td>
                                <td>Weekdays：07:00-05:59</td>
                                <td>Weekdays：01:00-24:00</td>
                            </tr>               
                        </tbody>
                    </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>NUN remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>

        <div class="account-intro cfff tc hidden-xs">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">Account</h2>
                        <p class="cfff mt50 fs20">Now, Easy get your account type</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">Read More</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>