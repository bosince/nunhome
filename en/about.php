<!DOCTYPE HTML>
<html>

<head>
	
	<meta charset="utf-8">
	
	<title>About us - NUN</title>
	
	<!-- meta -->
	<meta name="description" content="NUN ">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
	
	<!-- favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
	
	<!-- load modernizer -->
	<script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>
	
	<style>
		@media (max-width: 1440px) {
		    .max1440-w920{width: 920px;}

		}
        .container p{
            line-height: 30px;
            font-size: 15px;
        }

	</style>

</head>

<body>
	
	
	<div id="wrapper">
		
		<!-- header -->
		<?php include 'header.html'; ?>

		<div class="slide h300 bcfff tc pt120">
			<h2 class="cfff fs40  ffwryh">About us</h2>
		</div>


		
		<div  >
        <div class="container  ">
            <div class="ffwryh c666 mt80">
                <p>NUN is headquartered in Europe's largest economic center, London, UK, as the world's largest international foreign exchange market and the world's largest offshore dollar, euro marke; the dollar and the euro is pricing here, 41% of the global currency business was done in London.</p>

                <p>As the world's first centralized call options trading center; foreign exchange, CFD (CFDS), commodity futures licensed suppliers, has a perfect financial regulatory system and strict implementation.</p>

                <p>National Futures Association (NAF) financial supervision license, It has been recognized as the world's most stringent, the most sound, most investor protection financial regulatory system.</p>

                <p>If any accidents in the transaction, investors will obtain a compensation of fifty thousand pounds and have an opportunity to increase the level of the full compensation.</p>

                <p>To offer a better service for worldwide customers, NUN  international sets up representative offices all over UK, USA, Hongkong, Australia and other countries and regions around the world, offering more than ten kinds of major world languages online consulting and services.</p>

                <p>NUN has the world’s top liquidity providers, including Barclays Bank, the Morgan bank, Citibank, Bank of America, HSBC, Societe Generale AG, Goldman Sachs; etc..</p>

                <p>The core team of NUN is the industry's top financial practitioners, used to serve in international financial institutions, having strong professional value and rich financial experience.</p>

                <p>NUN insists independent research. Its trading platform includes personal computers, intelligent mobile phones, tablet PC and other terminals. Personalized interface, the safe and reliable guarantee are to let you enjoy the world's wealth of resources at ease without leaving home.</p>

                <p>NUN is committed to provide investors with one-stop service and top trading environment, accurate and reliable transparent quote, ensure that you get the best transaction value , the super fast account opening, smooth and rapid access to account Union Pay , quick execution, to let traders get maximum benefits.</p>

                <p>NUN believes that "Focus on one good thing for a hundred year”, adhering to customer first,  treating people with good faith, promoting the  financial derivatives investment market together. Let’s win an unlimited future and wealth all over the world!</p>
            </div>
            <div class="mt50 mb80 tc">
              <img src="assets/img/about/about_bg_en.png" alt="" class="w100-">
            </div>
        </div>
    </div>
		
		<!-- footer -->
		<?php include 'footer.html'; ?>
		
	</div> <!-- wrapper -->

	<!-- js -->
	<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

</body>
</html>