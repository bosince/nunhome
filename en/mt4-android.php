<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> Android NUN MT4 - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .lSuperiority li{line-height: 32px;}

        @media (max-width: 992px){
            .max992-ml0{margin-left: 0;}
            .max992-ml120{margin-left: 120px;}
            .max992-ml20-{margin-left: 20%;}
            .max992-tc{text-align: center;}
            .max992-w200{width: 200px;}
        }
    </style>

</head>

<body>
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="pt60 pb60">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-md-6 col-md-push-6">
                        <!-- 图片 -->
                        <figure>
                            <img src="assets/img/mt4/android/01.png" alt="" class="w100-">
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-pull-6">
                        <!-- 文字 -->
                        <div class=" ml30 max992-ml0 max992-tc ">   
                            <h2 class="fs30 fw7 lh35 mt50  ">Why NUN MT4 Droid Trader is Better?</h2>
                            <p class="mt15 ">The NUN MT4 Droid Trader allows you to access your account on an Android native application with the same login and password you use to access your account on your PC or Mac.</p>
                        </div>
                        <div class="ml30 max992-ml20- mt30">
                            <ul class="lSuperiority  ">    
                                <li><span class="glyphicon glyphicon-ok"></span> 100% Android Native Application</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Full MT4 Account Functionality</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 3 Chart Types</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 30 Technical Indicators</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Full Trading History Journal</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Real-time Interactive Charts with Zoom and Scroll</li>

                            </ul>
                            <a href="http://office.nunfx.com/Home/Reg/index.html" class="btn btn-success dib w300 max992-w200 h40 fw7 lh25">Open Live Account</a>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>
        <div class=" pt60 pb60 bcf1f1f1">   
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12"> 
                        <h2 class="fs25 fw7 lh50 ml30 mb15">How to Access NUN Android MT4</h2> 
                    </div>
                </div>
                <div class="row">   
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">Step 1</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Open Google Play on your Android, or <a href=" https://download.mql5.com/cdn/web/11104/mt4/nungroup4setup.exe" class="cc33">download the app here.</a></li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Locate Metatrader 4 in Google Play by entering the term metatrader 4 in the search field</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Click the Metatrader 4 icon to install the software to your Android</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">Step 2</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Now you will be prompted to select between Login with existing account /Open a demo account</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> On clicking either Login with Existing Account/Open a Demo Account, a new window opens</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Enter NUN in the search field</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Click the NUN -Demo icon if you have a demo account, or NUN -Real if you have a real account</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">Step 3</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Enter your login and password</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Start trading on your Android</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>