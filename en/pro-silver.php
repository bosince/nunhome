<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>XAGUSD - NUN</title>
    
    <!-- meta -->
    <meta name="description" content="NUN ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/silver01.jpg) no-repeat 0 50%;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/silver03.jpg) no-repeat 0 80%;background-size: cover;height: 500px;filter:alpha(opacity=90);-moz-opacity:0.9;-khtml-opacity: 0.9;  opacity: 0.9; }

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}

            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 650px;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}
        }
 

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">XAGUSD</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">XAGUSD</h2>
                        <p class="cfff mt50">In addition to gold and silver is another popular rare and precious metals, because of its scarcity and silver products increasingly rising market demand for silver. The silver market in the gold market is small, but is still a great investment value of the goods. And gold trading the same characteristics, silver trading is also carried out by security mechanism, and the formation of the shift in the currency composition and dollars. When the global economic situation because of the uncertainty and the dollar confidence, a good investment strategy is also to avoid the risk of holding the silver.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/silver02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why choose trading XAGUSD?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20 max768-fs16 c666">
                        <li>With a reserve value</li>
                        <li>The price fluctuation of profit opportunities</li>
                        <li>Can avoid the risk of US dollar</li>
                        <li>Time delivery, easy to be realized</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data</h2>
                <p class="tc plr15 mt30">NUN can provide the XAGUSD trading up to 100:1 for customers to leverage, at very competitive, the optimal bidding among international banks, contracts and flexible size, allows customers to enjoy multiple instruction and place an order risk management.</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>Transaction currency </td>
                            <td>Minimum number <br>of transactions</td>
                            <td>Maximum number<br> of transactions</td>
                            <td>Contract unit</td>
                            <td>Order distance</td>
                            <td>Bail scale </td>
                            <td>Business time<br>（Beijing time）</td>
                            <td>Business time<br>（MT4 time）</td>
                        </tr>
                        <tr>
                            <td>XAG/USD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>5,000</td>
                            <td>2.4</td>            
                            <td>0.5%~2%</td>
                            <td>Weekdays：07:00-05:59</td>
                            <td>Weekdays：01:00-24:00</td>
                        </tr>               
                    </tbody>
                </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>NUN remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>
        <div class="bc333 hidden-xs">
        <div class="account-intro c333 tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">Account</h2>
                        <p class="c333 mt50 fs20">Now, Easy get your account type</p>
                        <a href="#" class="dib mt50 c333 hov-c333">Read More</a>
                    </div>
                </div>
            </div>
        </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>