<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—法律条款</title>
    <meta name="keywords" content="法律条款,安全政策,风险,警告,反洗钱,赔偿基金 "/>
     <meta name="description" content="NUN只提供一般产品信息。因此，在申请账户之前，您一定要考虑好您的投资目标，财务状况和需求，以及在交易外汇保证金和差价合约时的高风险性。我们建议您咨询您的独立财务顾问、税务顾问和其他专业顾问。我们不能保证您在交易中的结果。 ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">


    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    
   <style>
        .art_style h3{line-height: 50px;}
        .art_style p{line-height: 30px;}
        .art_style a{line-height: 30px; font-size: 16px;}

    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  tc pt120">
            <h2 class="cfff  tc fs40  ffwryh">法律条款</h2>
        </div>


        
        <div class="container">
            <!-- <h2 class="fs30 fw7 pt30 mb30 lh60 c333 ffht  tc">法律条款</h2> -->
            <div class="ffwryh c666 art_style mt50 mb50">

                <p>NUN金融是受监管的外汇经纪商，并为广大用户提供全方位的专业在线交易平台。依照法规，NUN金融不会插手或涉及任何资管方面的事项，而是通过尖端科技为客户提供金融衍生品服务。</p>

                <p>当您交易外汇保证金和差价合约时，您要了解他们的复杂性，高风险性。它不一定适用于所有投资者。它取决于您是否适合交易外汇保证金和差价合约，您不应该在不了解这些交易风险之前就进行投资。如果您有任何疑问，去寻找一个客观、专业的建议是十分重要的。以下是一些交易差价合约的风险。产品服务指南中包含更多关于风险的信息，请确保在开户前，您已阅读过它。</p>

                <div class="panel-group mt15" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a class="dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          1.NUN金融不提供任何个人建议
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        我们只提供一般产品信息。因此，在申请账户之前，您一定要考虑好您的投资目标，财务状况和需求，以及在交易外汇保证金和差价合约时的高风险性。我们建议您仔细阅读 《产品披露声明》以及《金融服务指南》，然后咨询您的独立财务顾问、税务顾问和其他专业顾问。我们不能保证您在交易中的结果。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          2.您不是交易一个真实的资产
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        当您在交易外汇保证金和差价合约时，您的获利或损失源于交易产品价格的上涨或下跌。您需要了解您不是在买卖真实的外汇或期货指数合约黄金白银等大宗商品。您交易的是以价格波动为基础的差价合约，没有实际换汇或者大宗商品交易，属于金融衍生产品。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          3.场外交易（OTC）金融衍生产品
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        当您在我们的交易平台上进行交易时，您就是在进行场外交易（OTC）的衍生金融合约，订单是不能转移的。这就意味着您是直接和我们进行交易的，并且这些交易（仓位）只能在我们这结束。也就是说，外汇保证金和差价合约都是直接和我们发行机构交易的，不通过交易所，比如美国股票交易所。因此，您的获利与在其他市场中的获利无关。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          4.杠杆
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        当在进行外汇保证金交易时，您只需要一小部分保证金就可以开仓了。例如，如果您购买价值$10,000的AUD/USD，保证金比例为0.5%，那么您只需要$50就可以开仓了。但是，您在市场上的风险为$10,000。如果您的仓位盈利10%，您就获利$1000。如过您的仓位亏损10%，您就损失$1000。您交易的盈亏与您开启的仓位的大小有关。NUN金融杠杆来自于后台清算银行，为提供更好的优势增加/减少每个产品的保证金比例，因此NUN金融保留因银行提供的流动性和风险控制的需要更改杠杆的权利。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          5.市场的波动性
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                      <div class="panel-body">
                        我们交易平台上的价格和订单执行是根据交易所和市场上的价格和流动性，以及我们发行机构收集的数据所决定的。您交易产品的价格和点差可能在金融市场上快速变化波动。产品报价和点差（拉大）上任何的变动都会对您的账户资金和仓位产生直接的影响。价格波动会导致一种常见的现象叫做跳空，这是由于当前价格突然变成另一个级别的价格所导致的。这种现象可能是由一个意想不到的经济事件或市场公告产生的，尤其是当这些信息在交易时间外发生时。所以，您可能不会有机会在两个价格之间开仓或平仓。平台会在下一个最接近的市场价格帮您成交。您需要承担价格跳空所带来的风险，您的亏损有可能超出您的账户净资产，使账户余额变成负数。您有责任和义务回补账户负值，NUN金融保留追究账户清零所需资金的权利。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                          6.交易滑点
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                      <div class="panel-body">
                        滑点也是交易者在交易的过程中需要面对的风险。滑点是指由于快速的价格波动，交易者的实际成交价格和预设的成交价格不一致。NUN金融采取直接接入市场的成交模式，所有交易单子都需要后台清算银行确认价格后执行，所以交易者需务必了解滑点是交易中客观存在的现象。另外，挂单（包括挂单开仓或者挂单平仓）的价格仅供参考，最后成交价格以银行的实际成交价格为准。您需要了解，在快速波动的市场，挂单的实际成交结果有可能与您预设结果有差异。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                          7.强制平仓风险
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                      <div class="panel-body">
                        在任何时刻，您账户的现有资金一定要保持在强制平仓水平之上（预付款比例要保持在50%以上），否则您的开启仓位都将被逐个平仓。但是，请您不要完全依赖系统自动强平。您有责任通过交易平台对您的仓位、账户余额和账户价值进行即时管理。为了防止强行平仓，您应该在您的账户中存入充足的资金，防止任何交易中存在的潜在损失或风险。注意，即使您之前存入的资金在当时是充足的，由于市场的快速变化，也可能会迅速变得不充足。另外，在市场快速波动的情况下，即使相互对冲（锁仓）的单子也有可能因为价格的迅速波动导致锁仓的单子都被强平。记住一定要根据自己账户里面的资金水平，开启合适仓位。不要开启过大仓位使自己的防守空间不足。
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                          8.交易对手风险
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                      <div class="panel-body">
                        当您在我们公司开户并开设一个保证金仓位时，您购入一个交易合约，我们就是您的交易合约提供商。在这种情况下，交易存在的风险是我们在不可控制的情况下可能无法履行对您的合约义务。这可能是因为我们或我们自己的交易合作伙伴（比如为我们提供对冲的机构）出现违约。在不可控的因素影响下，我们不能保证您交易合约的履行义务。另外，如果您的交易方法与交易策略与我们的风险控制章程相违背时，我们有权拒绝履行合约义务的权利，包括您的交易赢利与代理返佣。
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNine">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                          9.客户资金风险
                        </a>
                      </h4>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                      <div class="panel-body">
                        所有客户的资金都是完全分开的。这意味着所有客户的钱都是放入独立的，与我们的运营资金分开放置。客户的存入资金是放在我们独立的信托账户进行维护和运营的。在任何不可控的情况出现下，您的资金仍然会承担风险。
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTen">
                      <h4 class="panel-title">
                        <a class="collapsed dib w100-" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                          10.技术风险和其他一些影响您交易的情况
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                      <div class="panel-body">
                        一些情况的发生可能使我们不能执行订单，或使您不能进入我们的交易平台,或无法实现入金即时到账。这包括，服务器系统错误或终止，维护阶段，网络连接问题，或一些您或我们无法控制的第三方错误（如，网络供应商，电力公司或入金银行）。我们在这些问题上已有一个应急解决措施，但是在遇到突发状况时您仍然可能无法登陆到交易平台或实现网上入金即时到账。这些技术风险和意外状况会在您想开仓或平仓时带来风险，您需要承担该风险导致的交易结果。
                      </div>
                    </div>
                  </div>

                </div>


                <!-- <div class="ml15">
                    <p><a href="assets/pdf/NUN金融关于订单执行政策.pdf" target="_bank">NUN金融关于订单执行政策</a></p>
                    <p><a href="assets/pdf/NUN金融关于反洗钱的规定.pdf" target="_bank">NUN金融关于反洗钱的规定</a></p>
                    <p><a href="assets/pdf/NUN金融关于风险揭示和警告通知.pdf" target="_bank">NUN金融关于风险揭示和警告通知</a></p>
                    <p><a href="assets/pdf/NUN金融关于客户投诉程序.pdf" target="_bank">NUN金融关于客户投诉程序</a></p>
                    <p><a href="assets/pdf/NUN金融关于客户问卷调查.pdf" target="_bank">NUN金融关于客户问卷调查</a></p>
                    <p><a href="assets/pdf/NUN金融关于利益冲突政策.pdf" target="_bank">NUN金融关于利益冲突政策</a></p>
                    <p><a href="assets/pdf/NUN金融关于投资者赔偿基金.pdf" target="_bank">NUN金融关于投资者赔偿基金</a></p>
                    <p><a href="assets/pdf/NUN金融关于网站使用条款.pdf" target="_bank">NUN金融关于网站使用条款</a></p>
                    <p><a href="assets/pdf/NUN金融关于隐私与安全政策.pdf" target="_bank">NUN金融关于隐私与安全政策</a></p>
                    <p><a href="assets/pdf/NUN金融为客户提供服务的条款和条件.pdf" target="_bank">NUN金融为客户提供服务的条款和条件</a></p>
                </div> -->
            </div> 
        </div>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <!-- <script type="text/javascript" src="assets/js/jquery/jquery.min.js"></script> -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>

</body>
</html>