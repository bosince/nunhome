<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—PC系统MT4</title>
    <meta name="keywords" content="PC,台式电脑,NUN MT4,下载,真实帐户,模拟帐户 "/>
     <meta name="description" content="NUN提供的PC系统MT4平台开创了高质量的执行订单。无重复报价、无拒绝订单，杠杆100:1。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .lSuperiority li{line-height: 32px;}

        @media (max-width: 992px){
            .max992-ml0{margin-left: 0;}
            .max992-ml120{margin-left: 120px;}
            .max992-ml20-{margin-left: 20%;}
            .max992-tc{text-align: center;}
        }
    </style>

</head>

<body>
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="pt60 pb60">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-md-6 col-md-push-6">
                        <!-- 图片 -->
                        <figure>
                            <img src="assets/img/mt4/pc/01.png" alt="" class="w100-">
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-pull-6">
                        <!-- 文字 -->
                        <div class=" ml30 max992-ml0 max992-tc">   
                            <h2 class="fs30 fw7 lh35 mt30  ">NUN MT4有什么优势?</h2>
                            <p class="mt15 ">NUN提供的MT4平台开创了高质量的执行订单。无重复报价、无拒绝订单，杠杆100:1。</p>
                        </div>
                        <div class="ml30 max992-ml20-">
                            <h3 class="fs20 lh35 mt30 ">NUN MT4特色</h3>
                            <ul class="lSuperiority  ">    
                                <li><span class="glyphicon glyphicon-ok"></span> 超过55种交易商品包括外汇、CFD和期货</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 点差低至0个点</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 完整EA (Expert Advisor)功能</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 一键交易</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 50个指标技术分析工具和图表功能</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 3种图表类型</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 允许对冲</li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <div class="row mt30 tc">   
                    <div class="lh50"><a href="http://office.nunfx.com/Home/Reg/index.html" class="btn btn-success dib w300 h40 fw7 lh25">建立真实账户</a></div>
                    <div class="lh50"><a href="https://download.mql5.com/cdn/web/11104/mt4/nungroup4setup.exe" class="btn btn-danger  dib w300 h40 fw7 lh25">下载</a></div>
                    <!-- <div class="lh50"><a href="#" class="btn btn-default dib w300 h40 fw7 lh25 cc33">建立模拟账户</a></div> -->
                </div>
            </div>
        </div>
        <div class=" pt60 pb60 bcf1f1f1">   
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 ">    
                        <h3 class="lh35 ml30">如何安装NUN MT4</h3>
                        <ul class="lSuperiority  ml30">    
                            <li class="cc33"><span class="glyphicon glyphicon-triangle-right cc33"></span> <a href="https://download.mql5.com/cdn/web/11104/mt4/nungroup4setup.exe">点击这里下载终端 (.exe file)</a></li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 下载完成后运行NUN.exe文件</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 第一次运行程序，您会看到登录窗口</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 输入真实账户或模拟账户的注册信息</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>