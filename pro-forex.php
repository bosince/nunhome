<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—外汇主流盘</title>
    <meta name="keywords" content="外汇主流盘,EURUSD,GBPUSD,USDJPY,AUDUSD,直盘 "/>
     <meta name="description" content="NUN能为客户提供28种外汇主流货币对，包括美元、欧元、英镑、日元、加元等，投资者完全可以根据交易习惯或经验选择一组或多组货币对进行分散投资。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/introduce.jpg);background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 45px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/03.jpg) no-repeat 0 79%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">外汇主流盘</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">外汇主流盘</h2>
                        <p class="cfff mt50">外汇市场是世界上交易量最大的金融市场，据国际清算银行（BIS）统计，2013年全球外汇日交易量已高达5.3万亿，并呈现持续增长趋势。相较于股票、债券等其他金融市场，外汇市场拥有24小时连续性交易、公正透明、高流通性、杠杆机制、交易成本低等优势特性，因而成为了人们竞相追捧的投资领域。NUN能为客户提供28种外汇主流货币对，包括美元、欧元、英镑、日元、加元等，投资者完全可以根据交易习惯或经验选择一组或多组货币对进行分散投资。</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl0 mt30 ">为什么选择外汇主流盘</h2>
                    <ul class="pl50 max768-pl0 mt50  fs20 c666">
                        <li>T+0多空双向选择</li>
                        <li>保证金机制，以小博大</li>
                        <li>低成本交易，无手续费</li>
                        <li>大资金很难操控市场</li>
                        <li>利润丰厚</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">NUN采用浮动点差，客户能在NUN平台中体验到令人兴奋的至低点差，且无重复报价。此外，NUN还能提供高达100：1的灵活杠杆，有效帮助交易者实现利益最大化。</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30 w100-" >
                    <tr>
                        <th>热门货币对</th>
                        <th>最小交易手数</th>
                        <th>最大交易手数</th>
                        <th>合约单位</th>
                        <th>挂单距离</th>
                        <th>保证金比例<br>（视账户净值及余额情况而定）</th>
                        <th>交易时间（北京时间）</th>
                        <th>交易时间（MT4时间）</th>
                    </tr>
                    <tr>
                        <td>EUR/USD</td>
                        <td>0.01</td>
                        <td>20</td>
                        <td>100,000</td>
                        <td>2.4</td>
                        <td>0.5%~2%</td>
                        <td>周一至周五：06:05-05:59</td>
                        <td>周一至周五：00:05-24:00</td>
                    </tr>
                    <tr>
                        <td>USD/JPY</td>
                        <td>0.01</td>
                        <td>20</td>
                        <td>100,000</td>
                        <td>2.4</td>
                        <td>0.5%~2%</td>
                        <td>周一至周五：06:05-05:59</td>
                        <td>周一至周五：00:05-24:00</td>
                    </tr>
                    <tr>
                        <td>GBP/USD</td>
                        <td>0.01</td>
                        <td>20</td>
                        <td>100,000</td>
                        <td>2.4</td>
                        <td>0.5%~2%</td>
                        <td>周一至周五：06:05-05:59</td>
                        <td>周一至周五：00:05-24:00</td>
                    </tr>
                    <tr>
                        <td>USD/CHF</td>
                        <td>0.01</td>
                        <td>20</td>
                        <td>100,000</td>
                        <td>2.4</td>
                        <td>0.5%~2%</td>
                        <td>周一至周五：06:05-05:59</td>
                        <td>周一至周五：00:05-24:00</td>
                    </tr>
                </table>
                </div>
                

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>NUN 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
                <div class="tc mt30">
                    <a href="/pro-forex-more.php" class="dib btn btn-default w200 h50 lh35" >查看更多货币对</a>
                </div>
            </div>
        </div>

        <div class="account-intro cfff tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">账户介绍</h2>
                        <p class="cfff mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">了解更多</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>