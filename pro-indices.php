<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—指数</title>
    <meta name="keywords" content="指数,CFD,差价合约"/>
     <meta name="description" content="指数即股票指数，是反映不同时期的股价变动情况的相对指标，其价格变化及因价格变化所带来的盈利或亏损由CFD（差价合约）来反映并提出。外汇交易中，指数是用保证金进行交易的，盈利或亏损皆由买入和卖出价格决定。相对于股票而言，指数交易不必让客户投入全部资金就可以进行自由买卖，可以双向多空交易，同时享有市场波动带来的全部收益和风险。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/indices01.jpg) no-repeat ;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 42px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/indices03.jpg) no-repeat 0 25%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">指数</li>
            </ol>
        </div>
        <div class="introduce  ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="c333 tc">指数</h2>
                        <p class="c333 mt50">指数即股票指数，是反映不同时期的股价变动情况的相对指标，其价格变化及因价格变化所带来的盈利或亏损由CFD（差价合约）来反映并提出。外汇交易中，指数是用保证金进行交易的，盈利或亏损皆由买入和卖出价格决定。相对于股票而言，指数交易不必让客户投入全部资金就可以进行自由买卖，可以双向多空交易，同时享有市场波动带来的全部收益和风险。NUN能为客户提供世界颇有影响力的六大指数包括道·琼斯指数、标普指数、纳斯达克指数、英国富时100指数、德国DAX30指数、法国CAC40指数</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/indices02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl0 mt30 ">为什么选择交易指数？</h2>
                    <ul class="pl50 max768-pl0 mt50  fs20 c666">
                        <li>执行迅捷，无重复报价</li>
                        <li>极具竞争力的点差</li>
                        <li>通过卖空交易进行风险对冲</li>
                        <li>无印花税等，成本低</li>
                        <li>多重下单方式，杠杆灵活</li>
                        <li>赚取股息</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">NUN平台可以让客户的订单瞬间进入国际外汇市场，并享受顶级的市场流通性与合理的点差，杠杆灵活，且交易成本低，无需支付结算费用，通过买卖差价赚取利润。</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>产品名称</td>
                            <td>最小交易手数</td>
                            <td>最大交易手数</td>
                            <td>合约单位</td>
                            <td>挂单距离</td>
                            <td>保证金比例</td>
                            <td>交易时间（北京时间）</td>
                            <td>交易时间（MT4时间）</td>
                        </tr>
                        <tr>
                            <td>U30USD</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>2%</td>
                            <td>周一至周四：07:00-05:15  05:30-05:59<br>周五：07:00-05:15</td>
                            <td>周一至周四：01:00-23:15  23:30-24:00<br>周五：01:00-23:15</td>
                        </tr>
                        <tr>
                            <td>SPXUSD</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>100</td>
                            <td>2.4</td>
                            <td>2%</td>
                            <td>周一至周四：07:00-05:15  05:30-05:59<br>周五：07:00-05:15</td>
                            <td>周一至周四：01:00-23:15  23:30-24:00<br>周五：01:00-23:15</td>
                        </tr>
                        <tr>
                            <td>NASUSD</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>2%</td>
                            <td>周一至周四：07:00-05:15  05:30-05:59<br>周五：07:00-05:15</td>
                            <td>周一至周四：01:00-23:15  23:30-24:00<br>周五：01:00-23:15</td>
                        </tr>
                        <tr>
                            <td>100GBP</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>4%</td>
                            <td>周一至周五：15:00-05:00</td>
                            <td>周一至周五：09:00-23:00</td>
                        </tr>
                        <tr>
                            <td>D30EUR</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>4%</td>
                            <td>周一至周五：15:00-05:00</td>
                            <td>周一至周五：09:00-23:00</td>
                        </tr>
                        <tr>
                            <td>F40EUR</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>4%</td>
                            <td>周一至周五：15:00-05:00</td>
                            <td>周一至周五：09:00-23:00</td>
                        </tr>
                        <tr>
                            <td>H33HKD</td>
                            <td>0.1</td>
                            <td>10</td>
                            <td>10</td>
                            <td>2.4</td>
                            <td>2%</td>
                            <td>周一至周五：09:15-12:00  13:15-16:15</td>
                            <td>周一至周五：04:15-07:00  08:15-11:15</td>
                        </tr>
                        <tr>
                            <td>300CNH</td>
                            <td>0.01</td>
                            <td>10</td>
                            <td>300</td>
                            <td>500</td>
                            <td>2%</td>
                            <td>周一至周五：09:30-11:30  13:00-15:00</td>
                            <td>周一至周五：03:30-05:30  07:00-09:00</td>
                        </tr>
                        <tr>
                            <td>HK50</td>
                            <td>1</td>
                            <td>1000</td>
                            <td>50</td>
                            <td>1000</td>
                            <td>3.3%</td>
                            <td>周一至周五：01:00-04:00  05:00-08:30<br>09:00-15:45</td>
                            <td>周一至周五：01:00-04:00  05:00-08:30<br>  09:00-15:45</td>
                        </tr>
                    </tbody>
                </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>NUN 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>

        <div class="account-intro cfff tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">账户介绍</h2>
                        <p class="cfff mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">了解更多</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>