<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—MT4 iPHONE终端</title>
    <meta name="keywords" content="iPHONE,苹果手机,NUN MT4,下载,真实帐户,模拟帐户 "/>
    <meta name="description" content="使用与您PC或Mac系统账户相同的账号和密码登陆到NUN MT4 iPhone终端。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .lSuperiority li{line-height: 32px;}

        @media (max-width: 992px){
            .max992-ml0{margin-left: 0;}
            .max992-ml120{margin-left: 120px;}
            .max992-ml20-{margin-left: 20%;}
            .max992-tc{text-align: center;}
             .max992-w200{width: 200px;}
        }
    </style>

</head>

<body>
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="pt60 pb60">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-md-6 col-md-push-6">
                        <!-- 图片 -->
                        <figure>
                            <img src="assets/img/mt4/iphone/01.png" alt="" class="w100-">
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-pull-6">
                        <!-- 文字 -->
                        <div class=" ml30 max992-ml0 max992-tc ">   
                            <h2 class="fs30 fw7 lh35 mt50  ">NUN MT4 iPhone终端有什么优势?</h2>
                            <p class="mt15 ">使用与您PC或Mac系统账户相同的账号和密码登陆到NUN MT4 iPhone终端。</p>
                        </div>
                        <div class="ml30 max992-ml20- ">
                            <h3 class="fs20 lh35 mt30 ">NUN MT4 iPhone终端特色</h3>
                            <ul class="lSuperiority  ">    
                                <li><span class="glyphicon glyphicon-ok"></span> 100% iPad原生应用</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 完整的MT4账户功能</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 3种图表类型</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 30种技术指标</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 完整的交易历史日志</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 内置可推送新闻功能</li>

                            </ul>
                            <a href="http://office.nunfx.com/Home/Reg/index.html" class="btn btn-success dib w300 max992-w200 h40 fw7 lh25">建立真实账户</a>
                        </div>
                        
                    </div>
                </div>
<!--                 <div class="row mt30 tc">   
                    <div class="lh50"><a href="#" class="btn btn-success dib w300 h40 fw7 lh25">建立账户</a></div>
                    <div class="lh50"><a href="#" class="btn btn-danger  dib w300 h40 fw7 lh25">下载</a></div>
                    <div class="lh50"><a href="#" class="btn btn-default dib w300 h40 fw7 lh25 cc33">建立模拟账户</a></div>
                </div> -->
            </div>
        </div>
        <div class=" pt60 pb60 bcf1f1f1">   
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12"> 
                        <h2 class="fs25 fw7 lh50 ml30 mb15">如何使用NUN iPhone MT4</h2> 
                    </div>
                </div>
                <div class="row">  
                    
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">步骤1</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 打开您的应用商店，或者在这里<a href=" https://download.mql5.com/cdn/web/11104/mt4/nungroup4setup.exe" class="cc33">下载应用。</a></li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 在应用商店搜索栏里输入metatrader 4找到Metatrader 4</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 点击Metatrader 4图标安装软件到您的苹果系统</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">步骤2</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 现在您会看到提示，选择登陆到现有账户/建立模拟账户</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 单击任何一个会出现一个新窗口</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 在搜索栏里面输入NUN</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 如果您登陆模拟账户，点击NUN -Demo图标；如果您登陆真实账户，选择NUN -Real</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-4">    
                        <h3 class="lh35 ml30">步骤3</h3>
                        <ul class="lSuperiority  ml30">    
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 输入您的账号和密码</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> 开始在您的iPhone上交易</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>