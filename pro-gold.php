<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> NUN—现货黄金</title>
    <meta name="keywords" content="现货黄金,XAUUSD"/>
     <meta name="description" content=" 黄金是一种重要的金融商品同时也是稀有金属，具有非常高的经济价值。在金融动荡时期，黄金是投资者规避风险的高系数选择。外汇市场中的现货黄金可以不需要持实物进行交割，采用透过保证金的方式以美元相对的报价形式进行买入或卖出。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/gold01.jpg) no-repeat 0 50%;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/gold03.jpg) no-repeat 0 80%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">现货黄金</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">现货黄金</h2>
                        <p class="cfff mt50">黄金是一种重要的金融商品同时也是稀有金属，具有非常高的经济价值。在金融动荡时期，黄金是投资者规避风险的高系数选择。外汇市场中的现货黄金可以不需要持实物进行交割，采用透过保证金的方式以美元相对的报价形式进行买入或卖出。鉴于黄金与美元此消彼长的关系，许多投资者会选择以买入黄金的方式来规避美元风险。</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/gold02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50  mt30 ">为什么选择交易现货黄金？</h2>
                    <ul class="pl50  mt50  fs20 c666">
                        <li>具有储备保值功能</li>
                        <li>稳定安全，低风险，高回报</li>
                        <li>可作为避险策略</li>
                        <li>随时交收，容易变现</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">NUN可为客户提供黄金交易最高达100:1的杠杆，合理的点差，国际银行间最优的报价，及灵活的合约大小，并让客户享受多重指令与挂单的风险管理。</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>交易货币对</td>
                            <td>最小交易手数</td>
                            <td>最大交易手数</td>
                            <td>合约单位</td>
                            <td>挂单距离</td>
                            <td>保证金比列<br>（视账户净值及余额情况而定）</td>            
                            <td>交易时间（北京时间）</td>
                            <td>交易时间（MT4时间）</td>
                        </tr>
                        <tr>
                            <td>XAU/USD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100</td>
                            <td>2.4</td>            
                            <td>0.5%~2%</td>
                            <td>周一至周五：07:00-05:59</td>
                            <td>周一至周五：01:00-24:00</td>
                        </tr>               
                    </tbody>
                </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>NUN 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>

        <div class="account-intro cfff tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">账户介绍</h2>
                        <p class="cfff mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">了解更多</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>